## Java ##

Argomenti in orgine crescente di complessitÓ. Se trovate una parte troppo elementare fate un solo esercizio e passate alla successiva.

Solo il paragrafo 11 da https://materiaalit.github.io/2013-oo-programming/part1/week-2/  
Solo il paragrafo 17 da https://materiaalit.github.io/2013-oo-programming/part1/week-3/  
Solo i paragrafi 25, 26 e 30 da https://materiaalit.github.io/2013-oo-programming/part1/week-6/

### Style guide ###

TODO

## Spring Cloud

https://www.youtube.com/watch?v=aO3W-lYnw-o

### Http Client

https://github.com/OpenFeign/feign
https://dzone.com/articles/the-netflix-stack-using-spring-boot-part-3-feign

## MS Sql

### Date format

https://docs.microsoft.com/it-it/sql/t-sql/data-types/datetime2-transact-sql  
https://docs.microsoft.com/it-it/sql/t-sql/functions/cast-and-convert-transact-sql  

The CONVERT(...) function ignores format when converting varchar to date. It only uses it for converting date to varchar.  
Standard SQL format for dates is extended ISO 8601 (http://support.sas.com/documentation/cdl/en/lrdict/64316/HTML/default/viewer.htm#a003169814.htm).

The advantage in using the ISO 8601 format is that it is an international standard with unambiguous specification. Also, this format is not affected by the SET DATEFORMAT or SET LANGUAGE setting (ex. 2004-05-23T14:25:10, 2004-05-23T14:25:10.487).

## Mongo DB ##

https://docs.mongodb.com/manual/

## Git ##

https://www.atlassian.com/git/tutorials  
